﻿
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace geducSite.Models
{
    public class Util
    {
        public static bool ValidarAcesso(string perfilAcesso, string pagina)
        {
            UtilDAO ud = new UtilDAO();
            string perfilPagina = ud.pegarPerfilAcessoTela(pagina);
            bool podeAcessar = false;
            int tamanho = Convert.ToInt32(perfilPagina.Length);

            int i; 
            for (i = 0; i < tamanho; i++)
            {
                if (perfilPagina[i].Equals(perfilAcesso[i])) 
                    podeAcessar = true;
            }
            return podeAcessar;
        }

    }

}