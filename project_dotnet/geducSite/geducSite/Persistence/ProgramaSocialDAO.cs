﻿using geducSite.Context;
using geducSite.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class ProgramaSocialDAO : Conexao
    {

        public void CadastrarProgramaSocial(ProgramaSocial ps)
        {
            try
            {
                AbrirConexao();


                myCmd = new MySqlCommand("INSERT INTO programaSocial(nomePrograma, descricao, ambitoAdm) VALUES (@v1,@v2,@v3)", myCon);
                myCmd.Parameters.AddWithValue("@v1", ps.nomePrograma);
                myCmd.Parameters.AddWithValue("@v2", ps.descricao);
                myCmd.Parameters.AddWithValue("@v3", ps.ambitoAdm);
                myCmd.ExecuteNonQuery();

                cmd = new SqlCommand("INSERT INTO programaSocial(nomePrograma, descricao, ambitoAdm) VALUES (@v1,@v2,@v3)", con);
                cmd.Parameters.AddWithValue("@v1", ps.nomePrograma);
                cmd.Parameters.AddWithValue("@v2", ps.descricao);
                cmd.Parameters.AddWithValue("@v3", ps.ambitoAdm);
                cmd.ExecuteNonQuery();

            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }


        public DataTable preencherDropPrograma()
        {
            try
            {
                AbrirConexao();

                string sql = "SELECT idProgramaSocial, nomePrograma FROM programaSocial;";

                SqlDataAdapter sqlad = new SqlDataAdapter(sql, con);
                DataTable dt = new DataTable();

                sqlad.Fill(dt);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public List<ProgramaSocial> listar()
        {
            try
            {
                AbrirConexao();
                cmd = new SqlCommand("Select * from programaSocial ", con);
                dr = cmd.ExecuteReader();

                List<ProgramaSocial> lista = new List<ProgramaSocial>();
                while (dr.Read())
                {
                    ProgramaSocial ps = new ProgramaSocial();
                    ps.idProgramaSocial = Convert.ToInt32(dr["idProgramaSocial"]);
                    ps.nomePrograma = Convert.ToString(dr["nomePrograma"]);
                    ps.descricao = Convert.ToString(dr["descricao"]);
                    ps.ambitoAdm = Convert.ToString(dr["ambitoAdm"]);
                    lista.Add(ps);
                }
                return lista;
            }
            finally
            {
                FecharConexao();
            }
        }
    }
}