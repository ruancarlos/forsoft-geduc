﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class consulta_nota : System.Web.UI.Page
    {
        protected IEnumerable<Nota> TodasAsNotas;
        protected IEnumerable<Nota> retorno;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["TodasAsNotas"] != null)
            {
                TodasAsNotas = (IEnumerable<Nota>)Session["TodasAsNotas"];
            }
            else
            {
                TodasAsNotas = new NotaDAO().Listar();
                Session["TodasAsNotas"] = TodasAsNotas;
                Session.Timeout = 6000;
            }

        }


        protected IEnumerable<Nota> BuscarNotas()
        {
            int valor = Convert.ToInt32(ddlTipoDeBusca.SelectedValue);
            if (txtBuscar.Text.Length != 0)
            {
                switch (valor)
                {
                    case 1:
                        retorno = TodasAsNotas.Where(x => x.aluno.matricula.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 2:
                        retorno = TodasAsNotas.Where(x => x.nota == Convert.ToInt32(txtBuscar.Text));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 3:
                        retorno = TodasAsNotas.Where(x => x.disciplina.nome.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 4:
                        retorno = TodasAsNotas.Where(x => Convert.ToString(x.data).ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        DivBusca.Visible = true;
                        break;
                }
            }
            return retorno;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarNotas();
        }

    }
}