﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class cadastro_programaSocial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "cadastro_programasocial.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
            //fim session loginss

        }
        protected void btnCadastrarProgSoci_Click(object sender, EventArgs e)
        {
            ProgramaSocial ps = new ProgramaSocial();
            ps.nomePrograma = txtNome.Text;
            ps.descricao = txtDescricao.Text;
            ps.ambitoAdm = txtAmbito.Text;

            ProgramaSocialDAO psd = new ProgramaSocialDAO();
            psd.CadastrarProgramaSocial(ps);

            msg.Text = "cadastrado com sucesso";

            txtNome.Text = "";
            txtDescricao.Text = "";
            txtAmbito.Text = "";
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            txtNome.Text = "";
            txtDescricao.Text = "";
            txtAmbito.Text = "";
        }
    }
}