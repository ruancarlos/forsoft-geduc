﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class cadastro_responsavel : System.Web.UI.Page
    {
        int idAluno;

        protected void Page_Load(object sender, EventArgs e)
        {
            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "cadastro_responsavel.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
            //
            //
            //
            //fim session loginss
            //
            //
            //
            //

            if (!Page.IsPostBack)
            {
                try
                {
                    ProgramaSocialDAO psd = new ProgramaSocialDAO();
                    DataTable dtp = psd.preencherDropPrograma();
                    ddlProgramaSocial.DataSource = dtp;
                    ddlProgramaSocial.DataValueField = "idProgramaSocial";
                    ddlProgramaSocial.DataTextField = "nomePrograma";
                    ddlProgramaSocial.DataBind();

                }
                catch (Exception ex)
                {
                    msg.Text = ex.Message;
                }

            }
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            limparCampos();
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                Responsavel r = new Responsavel();
                r.contato = new Contato();
                r.endereco = new Endereco();
                r.login = new Models.Login();
                r.programaSocial = new ProgramaSocial();
                r.documento = new Documento();

                ResponsavelDAO rd = new ResponsavelDAO();

                //Login
                r.login.usuario = txtUsuario.Text;
                r.login.senha = txtSenha.Text;
                r.login.perfilAcesso = "Responsável";

                //Responsavel
                r.grauParentesco = txtParentesco.Text;
                r.responsavel = rbResponsavel.SelectedValue.ToString();

                //Dados Pessoais
                r.nome = txtNomeAluno.Text;
                r.dataNascimento = Convert.ToDateTime(txtDataNascimento.Text);
                r.sexo = rbSexo.SelectedValue;
                r.naturalidade = txtNaturalidade.Text;
                r.nacionalidade = txtNacionalidade.Text;
                r.nomePai = txtNomePai.Text;
                r.nomeMae = txtNomeMae.Text;
                r.etnia = ddlEtnia.SelectedValue;
                r.estadoCivil = ddlEstadoCivil.SelectedValue;
                r.nivelEscolaridade = ddlEscolaridade.SelectedValue;
                r.necessidadeEsp = rbNecessidadeEspecial.SelectedValue;

                //Documentação
                r.documento.cpf = txtCPF.Text;
                r.documento.rg = txtRG.Text;
                r.documento.dataExpedicao = Convert.ToDateTime(txtDataExpedicao.Text);
                r.documento.orgaoExpedidor = txtOrgao.Text;
                r.documento.numCertidao = txtNumCertidaoNascimento.Text;
                r.documento.livroCertidao = txtLivroCertidaoNascimento.Text;
                r.documento.folhaCertidao = txtFolhaCertidaoNascimento.Text;
                r.documento.dataEmiCertidao = Convert.ToDateTime(txtDataCertidaoNascimento.Text);
                r.documento.titEleitor = txtTituloEleitor.Text;
                r.documento.certReservista = txtCertificadoReservista.Text;

                //Endereco
                r.endereco.longradouro = txtLogradouro.Text;
                r.endereco.numero = txtNumero.Text;
                r.endereco.complemento = txtComplemento.Text;
                r.endereco.bairro = txtBairro.Text;
                r.endereco.cidade = txtCidade.Text;
                r.endereco.uf = ddlUF.SelectedValue;
                r.endereco.cep = txtCEP.Text;
                r.endereco.municipio = txtMunicipio.Text;
                r.endereco.zona = txtZona.Text;

                //Contato
                r.contato.telefoneFixo = txtTelefone.Text;
                r.contato.telefoneCelular = txtCelular.Text;
                r.contato.outros = txtOutros.Text;
                r.contato.email = txtEmail.Text;

                //Programa Social
                r.programaSocial.idProgramaSocial = Convert.ToInt32(ddlProgramaSocial.SelectedValue);

                //Salvando o Aluno
                rd.salvar(r, Convert.ToInt32(lblIdAluno.Text));

                limparCampos();

                msg.Text = "O responsável " + r.nome + ", foi cadastrado com sucesso!";
            }
            catch (Exception ex)
            {
                msg.Text = ex.Message;
            }
        }

        private void limparCampos()
        {
            //Login
            txtUsuario.Text = null;
            txtSenha.Text = null;

            //Dados Pessoais
            txtNomeAluno.Text = null;
            txtNaturalidade.Text = null;
            txtNacionalidade.Text = null;
            txtNomePai.Text = null;
            txtNomeMae.Text = null;
            ddlEtnia.SelectedIndex = 0;
            ddlEstadoCivil.SelectedIndex = 0;
            ddlEscolaridade.SelectedIndex = 0;

            //Documentação
            txtCPF.Text = null;
            txtRG.Text = null;
            txtOrgao.Text = null;
            txtNumCertidaoNascimento.Text = null;
            txtLivroCertidaoNascimento.Text = null;
            txtFolhaCertidaoNascimento.Text = null;
            txtTituloEleitor.Text = null;
            txtCertificadoReservista.Text = null;

            //Endereco
            txtLogradouro.Text = null;
            txtNumero.Text = null;
            txtComplemento.Text = null;
            txtBairro.Text = null;
            txtCidade.Text = null;
            ddlUF.SelectedIndex = 0;
            txtCEP.Text = null;
            txtMunicipio.Text = null;
            txtZona.Text = null;

            //Contato
            txtTelefone.Text = null;
            txtCelular.Text = null;
            txtOutros.Text = null;
            txtEmail.Text = null;

            //Programa Social
            ddlProgramaSocial.SelectedIndex = 0;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            
            try
            {
                AlunoDAO ad = new AlunoDAO();
                Aluno a = new Aluno();

                int valor = Convert.ToInt32(ddlTipoDeBusca.SelectedValue);

                switch (valor)
                {
                    case 1:
                        a = ad.buscarAlunoMatricula(txtBuscar.Text);
                        aluno.Visible = true;
                        lblAluno.Text = a.nome;
                        idAluno = a.idAluno;
                        break;

                    case 2:
                        a = ad.buscarAlunoCpf(txtBuscar.Text);
                        aluno.Visible = true;
                        lblAluno.Text = a.nome;
                        idAluno = a.idAluno;
                        break;

                    case 3:
                        a = ad.buscarAlunoRg(txtBuscar.Text);
                        aluno.Visible = true;
                        lblAluno.Text = a.nome;
                        idAluno = a.idAluno;
                        break;
                }

                lblIdAluno.Text = Convert.ToString(idAluno);
            }
            catch (Exception ex)
            {
                msg.Text = ex.Message;
            }
        }
    }
}