﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class cadastro_disciplina : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "cadastro_disciplina.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
            //
            //
            //
            //fim session loginss
            //
            //
            //
            //
        }
        protected void btnCadastrar_Click(object sender, EventArgs e)
        {



            if (txtNomeDisciplina.Text != "" || txtCodigoDisciplina.Text != "" || txtCargaHorariaDisciplina.Text != "")
            {

                Disciplina dis = new Disciplina(); 
                dis.nome = txtNomeDisciplina.Text;
                dis.codigo = txtCodigoDisciplina.Text;
                dis.cargaHoraria = Convert.ToInt32(txtCargaHorariaDisciplina.Text);

                new DisciplinaDAO().Cadastrar(dis);

                msg.InnerText = "Cadastrado com sucesso!!!";
            }
            else
            {

                msg.InnerText = "Campo não preencido";
            }
            limpar();
        }

        public void limpar()
        {
            txtNomeDisciplina.Text = string.Empty;
            txtCodigoDisciplina.Text = string.Empty;
            txtCargaHorariaDisciplina.Text = string.Empty;
        }
    }
}